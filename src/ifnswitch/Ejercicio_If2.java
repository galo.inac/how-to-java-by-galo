package ifnswitch;

import java.util.Scanner;

public class Ejercicio_If2 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		int num, resto;
		
		System.out.println("Ingrese su n�mero: ");
		num = scan.nextInt();
		
		resto = num%2;

		if(resto==0) {
			System.out.println("Su n�mero es par :D");
		}
		else{
			System.out.println("Su n�mero es impar D:");
		}
		
		scan.close();
	}
}
