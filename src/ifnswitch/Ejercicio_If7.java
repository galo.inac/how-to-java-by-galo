package ifnswitch;

import java.util.Scanner;

public class Ejercicio_If7 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		int num1, num2, num3;
		
		System.out.println("Bienvenido al sistema selector de n�meros");
		System.out.print("Ingrese su primer n�mero: ");
		num1 = scan.nextInt();
		System.out.print("Ingrese su segundo n�mero: ");
		num2 = scan.nextInt();
		System.out.print("Ingrese su tercer n�mero: ");
		num3 = scan.nextInt();
		
		if(num1==num2) {
			if(num3>num1) {
				System.out.println("El tercer n�mero es el mayor");
			}
			else if(num3<num1){
				System.out.println("El primer y segundo n�mero son los mayores");
			}
			else {
				System.out.println("Los tres n�meros son iguales");
			}
		}
		else if(num1==num3) {
			if(num2>num1) {
				System.out.println("El segundo n�mero es el mayor");
			}
			else if(num2<num1) {
				System.out.println("El primer y tercer n�mero son los mayores");
			}
			else {
				System.out.println("Los tres n�meros son iguales");
			}
		}
		else if(num2==num3) {
			if(num1>num2) {
				System.out.println("El primer n�mero es el mayor");
			}
			else if(num1<num2) {
				System.out.println("El segundo y tercer n�mero son los mayores");
			}
			else {
				System.out.println("Los tres n�meros son iguales");
			}
		}
		else if(num1>num2) {
			if(num2>num3) {
				System.out.println("El primer n�mero es el mayor");
			}
			else if(num3>num2) {
				if(num3>num1) {
					System.out.println("El tercer n�mero es el mayor");
				}
				else {
					System.out.println("El primer n�mero es el mayor");
				}
			}
		}
		else if(num1>num3) {
			if(num3>num2) {
				System.out.println("El primer n�mero es el mayor");
			}
			else if(num2>num3) {
				if(num2>num1) {
					System.out.println("El segundo n�mero es el mayor");
				}
				else {
					System.out.println("El primer n�mero es el mayor");
				}
			}
		}
		else if(num3>num2) {
			if(num2>num1) {
				System.out.println("El tercer n�mero es el mayor");
			}
			else if(num1>num2) {
				if(num1>num3) {
					System.out.println("El primer n�mero es el mayor");
				}
				else {
					System.out.println("El tercer n�mero es el mayor");
				}
			}
		}
		else if(num2>num1) {
			if(num1>num3) {
				System.out.println("El segundo n�mero es el mayor");
			}
			else if(num3>num1) {
				if(num3>num2) {
					System.out.println("El tercer n�mero es el mayor");
				}
				else {
					System.out.println("El segundo n�mero es el mayor");
				}
			}
		}
		scan.close();
	}
}
