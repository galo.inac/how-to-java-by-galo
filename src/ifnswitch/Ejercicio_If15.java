package ifnswitch;

import java.util.Scanner;

public class Ejercicio_If15 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		char clase;
		
		System.out.println("---- Bienvenido al categorizador de veh�culos ----");
		System.out.println("Para utilizarlo, ingrese la clase que \nfigura en la patente de su veh�culo");
		System.out.println("<<<<Recuerde que las clases son: \"A\", \"B\", o \"C\">>>>");
		System.out.print("Coloque su categor�a ----> ");
		clase = scan.next().charAt(0);
		
		switch(clase) {
		case 'a': case 'A':
			System.out.println("Su veh�culo clase \"A\" cuenta con: ");
			System.out.println("\t� 4 ruedas\n\t� Un motor");
			break;
		case 'b': case 'B':
			System.out.println("Su veh�culo clase \"B\" cuenta con: ");
			System.out.println("\t� 4 ruedas\n\t� Un motor\n\t� Sistema de cerradura centralizada\n\t� Sistema de aire acondicionado");
			break;
		case 'c': case 'C':
			System.out.println("Su veh�culo clase \"C\" cuenta con: ");
			System.out.println("\t� 4 ruedas\n\t� Un motor\n\t� Sistema de cerradura centralizada\n\t� Sistema de aire acondicionado\n\t� Sistema de Airbag");
			break;
		default:
			System.out.println("Su clase no se encuentra en la base de datos");
			break;
		}
		
		scan.close();
	}

}
