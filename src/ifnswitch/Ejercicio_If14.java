package ifnswitch;

import java.util.Scanner;

public class Ejercicio_If14 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		int puesto;
		
		System.out.print("Ingrese su puesto en el torneo ----> ");
		puesto = scan.nextInt();
		
		switch(puesto) {
		case 1:
			System.out.println("Felicidades, usted gan� una medalla de ORO.");
			break;
		case 2:
			System.out.println("Genial, usted gan� una medalla de PLATA.");
			break;
		case 3:
			System.out.println("Asombroso, usted gan� una medalla de BRONCE.");
			break;
		default:
			System.out.println("Oh... usted deber�a seguir participando.");
		}
		scan.close();
	}
}
