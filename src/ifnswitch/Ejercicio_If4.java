package ifnswitch;

import java.util.Scanner;

public class Ejercicio_If4 {


	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Ingrese su categor�a: "); 
		String cat = scan.nextLine();

		if(cat.equals("a")) {
			System.out.println("La categor�a \"" + cat + "\" corresponde a hijo/a.");
		}
		else if(cat.equals("b")){
			System.out.println("La categor�a \"" + cat + "\" corresponde a padre/madre.");
		}
		else if(cat.equals("c")) {
			System.out.println("La categor�a \"" + cat + "\" corresponde a abuelo/abuela.");
		}
		else {
			System.out.println("ERROR:La categor�a \"" + cat + "\" es inexistente.");
		}
		scan.close();
	}
}