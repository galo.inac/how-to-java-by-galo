package ifnswitch;

import java.util.Scanner;

public class Ejercicio_If12 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		int num;
		
		System.out.println("---- Bienvenido al escaner de docenas ----");
		System.out.print("Ingrese su n�mero ---->");
		num = scan.nextInt();
		
		if(num>=1 && num<=12) {
			System.out.println("Su n�mero \"" + num + "\" se encuentra dentro de la primer docena");
		}
		else if(num>=13 && num<=24) {
			System.out.println("Su n�mero \"" + num + "\" se encuentra dentro de la segunda docena");
		}
		else if(num>=25 && num<=36) {
			System.out.println("Su n�mero \"" + num + "\" se encuentra dentro de la tercer docena");
		}
		else if(num<1 || num>36) {
			System.out.println("Su n�mero \"" + num + "\" se encuentra fuera rango");
		}
		scan.close();
	}

}
