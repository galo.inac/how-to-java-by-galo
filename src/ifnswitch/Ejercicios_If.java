package ifnswitch;

import java.util.Scanner;

public class Ejercicios_If {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		double nota1, nota2, nota3, promedio;
		
		System.out.println("Ingrese su primer nota: ");
		nota1 = scan.nextDouble();
		System.out.println("Ingrese su segunda nota: ");
		nota2 = scan.nextDouble();
		System.out.println("Ingrese su tercer nota: ");
		nota3 = scan.nextDouble();
		
		promedio = (nota1 + nota2 + nota3)/3;
		
		if(promedio>=7) {
			System.out.println("Felicidades! Aprobaste con " + promedio);
		}
		else {
			System.out.println("Oh... desaprobaste con " + promedio);
		}
		
		scan.close();
	}
}
