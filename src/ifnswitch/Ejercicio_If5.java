package ifnswitch;

import java.util.Scanner;

public class Ejercicio_If5 {

	public static void main(String[] args) {
		
		
		Scanner scan = new Scanner(System.in);
		
		int puesto;
		
		System.out.print("Ingrese su puesto en el torneo: ");
		puesto = scan.nextInt();
		
		if(puesto==1) {
			System.out.println("Felicidades!!! Usted sali� " + puesto + "� y se lleva la medalla de ORO.");
		}
		else if(puesto==2) {
			System.out.println("Genial!! Usted sali� " + puesto + "� y se lleva la medalla de PLATA.");
		}
		else if(puesto==3) {
			System.out.println("Bueno, salir " + puesto + "� no est� tan mal. Usted se lleva la medalla de BRONCE");
		}
		else {
			System.out.println("Siga participando para alcanzar a los mejores.");
		}
		scan.close();
	}
}
