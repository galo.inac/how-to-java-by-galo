package ifnswitch;

import java.util.Scanner;

public class Ejercicio_If16 {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		int num, i, result;
		result = 0;
		
		System.out.println("----- Bienvenido al sistema de tablas de multiplicaci�n -----");
		System.out.print("Ingrese aqu� el n�mero del cual quiere saber su tabla ----> ");
		num = scan.nextInt();
		
		for(i=1; i<=10; i++) {
			result=num*i;
			System.out.println(num + " * " + i + " = " + result);
		}
		scan.close();
	}
}
