package ifnswitch;

import java.util.Scanner;

public class Ejercicio_If13 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		
		System.out.println("---- Bienvenido al sistema reconocedor de meses ----");
		System.out.print("Ingrese el mes del cual quiere saber su cantidad de d�as ----> ");
		String mes = scan.nextLine();
		
		switch(mes){
		case "Febrero":
			System.out.println("Tiene 28 dias");
			break;
		case "Abril": case "Junio": case "Septiembre": case "Noviembre":
			System.out.println("Tiene 30 d�as");
			break;
		case "Enero": case "Marzo": case "Mayo": case "Julio": case "Agosto": case "Octubre": case "Diciembre":
			System.out.println("Tiene 31 d�as");
			break;
		default:
			System.out.println("Ingres� un mes incorrecto o no ingres� alguno.");
		}
		scan.close();
	}

}
