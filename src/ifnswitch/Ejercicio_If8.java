package ifnswitch;

import java.util.Scanner;

public class Ejercicio_If8 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		int jugador1, jugador2;
		
		System.out.println("----- Bienvenido a Rock, Paper, Scissors en JAVA -----");
		System.out.println("REGLAS: Las reglas son simples. Dos jugadores se enfrentan, cada uno");
		System.out.println("eligiendo una de las tres opciones: Piedra (0), Papel (1), o Tijeras (2).");
		System.out.println("Recordemos que: El Papel cubre a la Piedra, La Piedra rompe las Tijeras,\ny las Tijeras cortan Papel\n");
		System.out.print("Jugador 1 -----> �Qu� eliges? ");
		jugador1 = scan.nextInt();
		System.out.print("Jugador 2 -----> �Qu� eliges? ");
		jugador2 = scan.nextInt();
		
		if(jugador1==0) {
			if(jugador2==1) {
				System.out.println("Player 2, WINS!");
			}
			else if(jugador2==2) {
				System.out.println("Player 1, WINS!");
			}
			else {
				System.out.println("TIE!");
			}
		}
		else if(jugador1==1) {
			if(jugador2==0) {
				System.out.println("Player 1, WINS!");
			}
			else if(jugador2==2) {
				System.out.println("Player 2, WINS!");
			}
			else {
				System.out.println("TIE!");
			}
		}
		else if(jugador1==2) {
			if(jugador2==0) {
				System.out.println("Player 2, WINS!");
			}
			else if(jugador2==1) {
				System.out.println("Player 1, WINS!");
			}
			else {
				System.out.println("TIE!");
			}
		}
		scan.close();
	}
}
