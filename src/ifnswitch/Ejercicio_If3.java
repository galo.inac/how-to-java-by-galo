package ifnswitch;

import java.util.Scanner;

public class Ejercicio_If3 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Ingrese el mes: ");
		String mes = scan.nextLine();
		
		if (mes.equals("Febrero")) {
			System.out.println(mes + " tiene 28 d�as."); //�nico mes con 28 d�as
		}
		else if(mes.equals("Abril")){ 
			System.out.println(mes + " tiene 30 d�as."); //Grupo de meses con 30 d�as
		}
		else if (mes.equals("Junio")) {
			System.out.println(mes + " tiene 30 d�as.");
		}
		else if (mes.equals("Septiembre")) {
			System.out.println(mes + " tiene 30 d�as.");
		}
		else if (mes.equals("Noviembre")) {
			System.out.println(mes + " tiene 30 d�as.");
		}
		else if (mes.equals("Enero")) {
			System.out.println(mes + " tiene 31 d�as."); //Grupo de meses con 31 d�as
		}
		else if (mes.equals("Marzo")) {
			System.out.println(mes + " tiene 31 d�as.");
		}
		else if (mes.equals("Mayo")) {
			System.out.println(mes + " tiene 31 d�as.");
		}
		else if (mes.equals("Julio")) {
			System.out.println(mes + " tiene 31 d�as.");
		}
		else if (mes.equals("Agosto")) {
			System.out.println(mes + " tiene 31 d�as.");
		}
		else if (mes.equals("Octubre")) {
			System.out.println(mes + " tiene 31 d�as.");
		}
		else if (mes.equals("Diciembre")) {
			System.out.println(mes + " tiene 31 d�as.");
		}
		else {
			System.out.println("Error de ingreso");
		}
		scan.close();
	}
}
