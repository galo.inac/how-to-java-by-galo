package ifnswitch;

import java.util.Scanner;

public class Ejercicio_If6 {

	public static void main(String[] args) {
		
		int cursada;
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Ingrese el nombre del sujeto: ");
		String name = scan.nextLine();
		System.out.print("Ingrese los a�os de estudio del sujeto: ");
		cursada = scan.nextInt();
		
		if(cursada>=0) {
			if(cursada==0) {
				System.out.println("El sujeto " + name + " est� cursando el jard�n de infantes.");
			}
			else if(cursada>12){
				System.out.println("El sujeto " + name + " est� cursando una carrera");
			}
			else if(cursada<=12){
				if(cursada>=7){
					System.out.println("El sujeto " + name + " est� cursando el " + (cursada-6) + "� a�o de secundaria");
				}
				else if(cursada<=6) {
					if(cursada>=1) {
						System.out.println("El sujeto " + name + " est� cursando el " + cursada + "� a�o de primaria");
					}
					else {
						System.out.println("ERROR: el sujeto no estudi�/a, o ha ingresado un a�o incorrecto.");
					}
				}
			}
		}
		else {
			System.out.println("ERROR: el sujeto no estudi�/a, o ha ingresado un a�o incorrecto.");
		}
		scan.close();
	}
}
